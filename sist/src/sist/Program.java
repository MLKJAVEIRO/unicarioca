package sist;

import entidade.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class Program {

    public static void main(String[] args) {

        int numero;
        String nome, op;
        double Saldo;

        Scanner entrada = new Scanner(System.in);

        List<Conta> acc = new ArrayList<Conta>();

        System.out.println("Deseja criar uma conta ?");
        op = entrada.next();

        int i=0;
        while ("sim".equals(op)) {

            System.out.println("Qual o numero da conta?");
            numero = entrada.nextInt();

            entrada.nextLine();
            System.out.println("Nome do titular?");
            nome = entrada.nextLine();

            System.out.println("Saldo da conta");
            Saldo = entrada.nextDouble();

            System.out.println("Que tipo de conta gostaria de criar ?");
            op = entrada.next();
            switch (op) {
                case "Corrente":
                    acc.add(new ContaCorrente(numero, nome, Saldo));
                    System.out.println("Adicione a tarifa mensal em cima da conta");
                    double tarifa = entrada.nextDouble();
                    acc.get(i).Operacao(tarifa);
                    break;
                case "Poupanca":
                    acc.add(new ContaPoupanca(numero, nome, Saldo));
                    System.out.println("Insira a porcentagem de rendimento em cima da conta");
                    double rendimento = entrada.nextDouble();
                    acc.get(i).Operacao(rendimento);
                    break;
                default:
                    System.out.println("Tipo de conta n�o reconhecida");
            }
            System.out.println("Deseja criar outra conta?");
            op = entrada.next();
            i++;
        }
        entrada.close();
    }

}