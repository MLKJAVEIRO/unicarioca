package entidade;

public class ContaCorrente extends Conta {

    //Atributos
    public final STATUS tipo = STATUS.Corrente;

    // Constructor
    public ContaCorrente(int numero, String nome, double Saldo){
        super(numero, nome, Saldo);
    }

    //Metodo
    @Override
    public void Operacao(double tarifa){
        setSaldo(getSaldo() - tarifa);
    }
}
