package entidade;

public abstract class Conta {
    // Atributos

    private int numero;
    public String nome;
    private double Saldo;

    // Constructor

    public Conta(int numero, String nome, double Saldo){
        this.numero = numero;
        this.nome = nome;
        this.Saldo = Saldo;
    }

    // Getters e Setters


    public int getNumero() {
        return numero;
    }

    public String getNome() {
        return nome;
    }

    public double getSaldo() {
        return Saldo;
    }

    public void setSaldo(double Saldo) {
        this.Saldo = Saldo;
    }

    // Metodo
    public abstract void Operacao(double x);

}

