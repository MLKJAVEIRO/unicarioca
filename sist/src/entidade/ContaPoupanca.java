package entidade;

public class ContaPoupanca extends Conta {

    //Atributos
    public final STATUS tipo = STATUS.Poupanca;

    // Constructor
    public ContaPoupanca(int numero, String nome, double Saldo){
        super(numero, nome, Saldo);
    }

    //Metodo
    @Override
    public void Operacao(double rendimento){
        setSaldo(getSaldo() * (rendimento/100));
    }
}
